package sx.mxs.HCFChat;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class HCFChat extends JavaPlugin implements Listener {

    private static Chat chat = null;

    private Map<String, String> chatFormats = new HashMap<>();

    @Override
    public void onEnable() {
        if (!setupChat()) {
            getLogger().severe("There seems to be an issue with Vault, disabling");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        saveDefaultConfig();
        ConfigurationSection config = getConfig().getConfigurationSection("RankFormats");
        config.getKeys(false).forEach(key -> chatFormats.put(key, ChatColor.translateAlternateColorCodes('&', config.getString(key))));
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String format = chatFormats.get(chat.getPrimaryGroup(player));
        if (format != null) {
            event.setFormat(format);
        }
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        if (rsp != null) {
            chat = rsp.getProvider();
        }
        return chat != null;
    }

}